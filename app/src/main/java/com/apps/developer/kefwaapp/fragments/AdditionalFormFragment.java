package com.apps.developer.kefwaapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.apps.developer.kefwaapp.R;

public class AdditionalFormFragment extends Fragment {
    private View view=null;
    private EditText etHeightCM=null;
    private EditText etHeightM=null;
    private EditText etWeight=null;
    private EditText etSize=null;
    private CheckBox cbLeftFooter=null;
    private CheckBox cbRightFooter=null;
    private CheckBox cbBothFeet=null;
    private EditText etDateOfBirth=null;
    private EditText etPlaceOfBirth=null;
    private EditText etNationality=null;
    private Spinner sMaritalStatus=null;
    private EditText etNumberOfChildren=null;
    private Button btnFinish=null;
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle savedInstanceState){
        view=layoutInflater.inflate(R.layout.fragment_additional_form,viewGroup,false);
        initFields();
        return view;
    }
    public Button getBtnFinish(){
        return btnFinish;
    }
    public void initFields(){
        btnFinish=(Button)view.findViewById(R.id.btn_finish);
        etHeightCM=(EditText)view.findViewById(R.id.et_height_cm);
        etHeightM=(EditText)view.findViewById(R.id.et_height_m);
        etWeight=(EditText)view.findViewById(R.id.et_weight);
        etSize=(EditText)view.findViewById(R.id.et_size);
        cbLeftFooter=(CheckBox)view.findViewById(R.id.cb_left_footer);
        cbRightFooter=(CheckBox)view.findViewById(R.id.cb_right_footer);
        cbBothFeet=(CheckBox)view.findViewById(R.id.cb_both_feet);
        etDateOfBirth=(EditText) view.findViewById(R.id.et_date_of_birth);
        etPlaceOfBirth=(EditText)view.findViewById(R.id.et_place_of_birth);
        etNationality=(EditText)view.findViewById(R.id.et_nationality);
        sMaritalStatus=(Spinner)view.findViewById(R.id.s_marital_status);
        etNumberOfChildren=(EditText)view.findViewById(R.id.et_number_of_children);
    }

    public String getHeightCM(){
        return etHeightCM.getText().toString();
    }
    public String getHeightM(){
        return etHeightM.getText().toString();
    }
    public String getWeight(){
        return etWeight.getText().toString();
    }
    public String getSize(){
        return etSize.getText().toString();
    }
    public boolean isLeftFooter(){
        return cbLeftFooter.isSelected();
    }
    public boolean isRightFooter(){
        return cbRightFooter.isSelected();
    }
    public boolean isBothFeet(){
        return cbBothFeet.isSelected();
    }
    public String getDateOfBirth(){
        return etDateOfBirth.getText().toString();
    }
    public String getPlaceOfBirth(){
        return etPlaceOfBirth.getText().toString();
    }
    public String getNationality(){
        return etNationality.getText().toString();
    }
    public String getMaritalStatus(){
        return sMaritalStatus.getSelectedItem().toString();
    }
    public String getNumberOfChildren(){
        return etNumberOfChildren.getText().toString();
    }

}
