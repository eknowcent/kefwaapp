package com.apps.developer.kefwaapp.fragments;

import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apps.developer.kefwaapp.R;
import com.apps.developer.kefwaapp.constants.ConstantURL;
import com.esafirm.imagepicker.features.ImagePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MembershipFormFragment extends Fragment {
    private View view=null;
    private ImageButton btnPickImage=null;
    private Spinner sDivisions=null;
    private List<String> divisionList=new ArrayList<String>();
    private ArrayAdapter divisionListAdapter=null;
    private Spinner sClubs=null;
    private List<String> clubList=new ArrayList<String>();
    private ArrayAdapter clubListAdapter=null;
    private EditText etSurName=null;
    private EditText etFirstName=null;
    private RadioGroup rgPlayerOnLoan=null;
    private Spinner sDivision=null;
    private Spinner sClub=null;
    private EditText etContractStarts=null;
    private EditText etContractEnds=null;
    private RadioGroup rgFreeAgent=null;

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup,Bundle savedInstanceState){
        view=layoutInflater.inflate(R.layout.fragment_membership_form,viewGroup,false);
        initFields();
        pickImage();
        initDivisions();
        initClubs();

        return view;
    }
    public void initFields(){
        btnPickImage=(ImageButton)view.findViewById(R.id.btn_pick_image);
        sDivisions=(Spinner)view.findViewById(R.id.s_division);
        sClubs=(Spinner)view.findViewById(R.id.s_club);
        etSurName=(EditText)view.findViewById(R.id.et_surname);
        etFirstName=(EditText)view.findViewById(R.id.et_first_name);
        rgPlayerOnLoan=(RadioGroup)view.findViewById(R.id.rg_on_loan);
        etContractStarts=(EditText)view.findViewById(R.id.et_contract_starts);
        etContractEnds=(EditText)view.findViewById(R.id.et_contract_ends);
        rgFreeAgent=(RadioGroup)view.findViewById(R.id.rg_free_agent);
    }
    public String getSurName(){
        return etSurName.getText().toString();

    }
    public String getFirstName(){
        return etFirstName.getText().toString();
    }
    public String getDivision(){
        return sDivision.getSelectedItem().toString();
    }
    public String getClub(){
        return sClub.getSelectedItem().toString();
    }
    public String getContractStarts(){
        return etContractStarts.getText().toString();
    }
    public String getContractEnds(){
        return etContractEnds.getText().toString();
    }

    public void pickImage(){

        btnPickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.create(getActivity()).limit(1) // Activity or Fragment
                        .start();
            }
        });
    }
    public void initDivisions(){


// Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        String url = ConstantURL.URL_DIVISIONS;


// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try {
                            JSONArray jsonArray=new JSONArray(response);
                            for(int r=0;r<jsonArray.length();r++){
                                JSONObject jsonObject=jsonArray.getJSONObject(r);
                                divisionList.add(jsonObject.getString("name"));


                            }
                            divisionListAdapter=new ArrayAdapter(getActivity().getApplicationContext(),R.layout.spinner_item,divisionList);

                            sDivisions.setAdapter(divisionListAdapter);

                        } catch (JSONException e) {
                            Toast.makeText(getActivity().getApplicationContext(),"Please make sure you phone is connected "+e.getMessage(),Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
                Toast.makeText(getActivity().getApplicationContext(),"Please make sure you phone is connected : "+error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void initClubs(){


// Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        String url = ConstantURL.URL_CLUBS;


// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try {
                            JSONArray jsonArray=new JSONArray(response);
                            for(int r=0;r<jsonArray.length();r++){
                                JSONObject jsonObject=jsonArray.getJSONObject(r);
                                clubList.add(jsonObject.getString("name"));


                            }
                            clubListAdapter=new ArrayAdapter(getActivity().getApplicationContext(),R.layout.spinner_item,clubList);

                            sClubs.setAdapter(clubListAdapter);

                        } catch (JSONException e) {
                            Toast.makeText(getActivity().getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(getActivity().getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

}
