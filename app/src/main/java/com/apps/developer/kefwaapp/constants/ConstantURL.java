package com.apps.developer.kefwaapp.constants;

public class ConstantURL {
    public static String URL_BASE="http://mtkenyahub.com/kefwa/api/";
    public static String URL_DIVISIONS=URL_BASE+"divisions.php";
    public static String URL_CLUBS=URL_BASE+"clubs.php";
    public static String URL_POSITIONS=URL_BASE+"positions.php";
    public static String URL_NATIONAL_TEAMS=URL_BASE+"national_teams.php";
    public static String URL_MEMBER_REGISTRATION=URL_BASE+"post/members.php";
}
