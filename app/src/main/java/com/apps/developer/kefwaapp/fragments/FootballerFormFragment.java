package com.apps.developer.kefwaapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apps.developer.kefwaapp.R;
import com.apps.developer.kefwaapp.constants.ConstantURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FootballerFormFragment extends Fragment {
    private View view=null;
    private List<String> positionList=new ArrayList<>();
    private ArrayAdapter<String> positionListAdapter=null;
    private Spinner sPosition=null;
    private List<String> nationalTeamList=new ArrayList<>();
    private ArrayAdapter<String> nationalTeamListAdapter=null;
    private Spinner sNationalTeam=null;
    private RadioGroup rgHasAnAgemt=null;
    private CheckBox cbUnder16=null;
    private CheckBox cbUnder17=null;
    private CheckBox cbUnder18=null;
    private CheckBox cbUnder19=null;
    private CheckBox cb20Years=null;
    private EditText etOtherSelection=null;
    private EditText etOlympics =null;
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle savedInstanceState){
        view=layoutInflater.inflate(R.layout.fragment_footballer_form,viewGroup,false);
        initFields();
        initPositions();
        initNationalTeams();
        return view;
    }

    public void initFields(){
        sPosition=(Spinner)view.findViewById(R.id.s_position);
        sNationalTeam=(Spinner)view.findViewById(R.id.s_national_teams);
        rgHasAnAgemt=(RadioGroup)view.findViewById(R.id.rg_has_an_agent);
        cbUnder16=(CheckBox)view.findViewById(R.id.cb_under_16);
        cbUnder17=(CheckBox)view.findViewById(R.id.cb_under_17);
        cbUnder18=(CheckBox)view.findViewById(R.id.cb_under_18);
        cbUnder19=(CheckBox)view.findViewById(R.id.cb_under_19);
        cb20Years=(CheckBox)view.findViewById(R.id.cb_20_years);
        etOtherSelection=(EditText)view.findViewById(R.id.et_other_selections);
        etOlympics=(EditText)view.findViewById(R.id.et_olympics);
    }
    public void initPositions(){


// Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        String url = ConstantURL.URL_POSITIONS;


// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try {
                            JSONArray jsonArray=new JSONArray(response);
                            for(int r=0;r<jsonArray.length();r++){
                                JSONObject jsonObject=jsonArray.getJSONObject(r);
                                positionList.add(jsonObject.getString("name"));


                            }
                            positionListAdapter=new ArrayAdapter(getActivity().getApplicationContext(),R.layout.spinner_item,positionList);

                            sPosition.setAdapter(positionListAdapter);

                        } catch (JSONException e) {
                            Toast.makeText(getActivity().getApplicationContext(),"Please make sure you phone is connected "+e.getMessage(),Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
                Toast.makeText(getActivity().getApplicationContext(),"Please make sure you phone is connected : "+error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
    public void initNationalTeams(){


// Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        String url = ConstantURL.URL_NATIONAL_TEAMS;


// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try {
                            JSONArray jsonArray=new JSONArray(response);
                            for(int r=0;r<jsonArray.length();r++){
                                JSONObject jsonObject=jsonArray.getJSONObject(r);
                                nationalTeamList.add(jsonObject.getString("name"));


                            }
                            nationalTeamListAdapter=new ArrayAdapter(getActivity().getApplicationContext(),R.layout.spinner_item,nationalTeamList);

                            sNationalTeam.setAdapter(nationalTeamListAdapter);

                        } catch (JSONException e) {
                            Toast.makeText(getActivity().getApplicationContext(),"Please make sure you phone is connected "+e.getMessage(),Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
                Toast.makeText(getActivity().getApplicationContext(),"Please make sure you phone is connected : "+error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
    public String getPosition(){
        return sPosition.getSelectedItem().toString();
    }
    public String getNationalTeam(){
        return sNationalTeam.getSelectedItem().toString();
    }
    public boolean hasAnAgent(){
        return true;
    }
    public boolean isUnder16(){
        return cbUnder16.isChecked();
    }
    public boolean isUnder17(){
        return cbUnder17.isChecked();
    }
    public boolean isUnder18(){
        return cbUnder18.isChecked();
    }
    public boolean isUnder19(){
        return cbUnder19.isChecked();
    }
    public boolean is20Years(){
        return cb20Years.isChecked();
    }
    public String getOtherSelections(){
        return etOtherSelection.getText().toString();
    }
    public String getOlypmics(){
        return etOlympics.getText().toString();
    }




}
