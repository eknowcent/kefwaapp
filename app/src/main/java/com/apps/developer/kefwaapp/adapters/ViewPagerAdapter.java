package com.apps.developer.kefwaapp.adapters;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.apps.developer.kefwaapp.fragments.AdditionalFormFragment;
import com.apps.developer.kefwaapp.fragments.ContactFormFragment;
import com.apps.developer.kefwaapp.fragments.FootballerFormFragment;
import com.apps.developer.kefwaapp.fragments.MembershipFormFragment;


public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private MembershipFormFragment membershipFormFragment=new MembershipFormFragment();
    private ContactFormFragment contactFormFragment=new ContactFormFragment();
    private FootballerFormFragment footballerFormFragment=new FootballerFormFragment();
    private AdditionalFormFragment additionalFormFragment=new AdditionalFormFragment();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return membershipFormFragment;
                case 1:
                    return contactFormFragment;
            case 2:
                return footballerFormFragment;
            case 3:
                return additionalFormFragment;
                default:
                    return membershipFormFragment;

        }

    }

    public MembershipFormFragment getMembershipFormFragment(){
        return membershipFormFragment;
    }

    public ContactFormFragment getContactFormFragment() {
        return contactFormFragment;
    }

    public FootballerFormFragment getFootballerFormFragment() {
        return footballerFormFragment;
    }

    public AdditionalFormFragment getAdditionalFormFragment() {
        return additionalFormFragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Membership";
            case 1:
                return "Contact";
            case 2:
                return "Footballer's";
            case 3:
                return "Additional";
                default:
                    return "Membership";
        }

    }


}
