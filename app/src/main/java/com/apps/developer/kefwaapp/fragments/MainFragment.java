package com.apps.developer.kefwaapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.apps.developer.kefwaapp.R;
import com.apps.developer.kefwaapp.activities.AboutUsActivity;
import com.apps.developer.kefwaapp.activities.RegistrationFormActivity;

public class MainFragment extends Fragment {
    private View view=null;
    private Button btnRegistration=null;
    private Button btnAboutUs=null;
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle savedInstanceState){
        view=layoutInflater.inflate(R.layout.fragment_main,viewGroup,false);
        initRegistration();
        initAboutUs();
        return view;
    }
    public void initRegistration(){
        btnRegistration=(Button)view.findViewById(R.id.btn_registration);
        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity().getApplicationContext(), RegistrationFormActivity.class);
                startActivity(intent);
            }
        });
    }
    public void initAboutUs(){
        btnAboutUs=(Button)view.findViewById(R.id.btn_about_us);
        btnAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity().getApplicationContext(), AboutUsActivity.class);
                startActivity(intent);
            }
        });
    }

}
