package com.apps.developer.kefwaapp.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apps.developer.kefwaapp.R;
import com.apps.developer.kefwaapp.adapters.ViewPagerAdapter;
import com.apps.developer.kefwaapp.constants.ConstantURL;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RegistrationFormActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private ViewPagerAdapter viewPagerAdapter=new ViewPagerAdapter(getSupportFragmentManager());

    private String firstName="";
    private String surName="";
    private boolean playerOnLoan=false;
    private String division="";
    private String club="";
    private String contractStarts="";
    private String contractEnds="";
    private boolean freeAgent=false;
    private String address="";
    private String city="";
    private String postalCode="";
    private String gsm="";
    private String country="";
    private String email="";
    private String phoneNumber="";
    private String fax="";
    private String twitter="";
    private String other="";
    private String facebook="";
    private boolean playerIsMinor=false;
    private boolean hasAnAgent=false;
    private String position="";
    private String nationalTeam="";
    private boolean under16=false;
    private boolean under17=false;
    private boolean under18=false;
    private boolean under19=false;
    private boolean _20years=false;
    private String otherSelection="";
    private String olympic="";
    private String heightM="";
    private String heightCM="";
    private String Size="";
    private String weight="";
    private boolean leftFooter=false;
    private boolean rightFooter=false;
    private boolean bothFeet=false;
    private String dateOfBirth="";
    private String nationality="";
    private String placeOfBirth="";
    private String maritalStatus="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_form);



        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Membership"));
        tabLayout.addTab(tabLayout.newTab().setText("Contact"));
        tabLayout.addTab(tabLayout.newTab().setText("Footballer's"));
        tabLayout.addTab(tabLayout.newTab().setText("Additional"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPagerAdapter.getAdditionalFormFragment().getBtnFinish().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registration();
            }
        });

    }
    public void registration(){
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = ConstantURL.URL_MEMBER_REGISTRATION;


// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try {
                            JSONArray jsonArray=new JSONArray(response);
                            for(int r=0;r<jsonArray.length();r++){
                                JSONObject jsonObject=jsonArray.getJSONObject(r);



                            }

                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),"Please make sure you phone is connected "+e.getMessage(),Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
                Toast.makeText(getApplicationContext(),"Please make sure you phone is connected : "+error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("sur_name",viewPagerAdapter.getMembershipFormFragment().getSurName());
                params.put("first_name",viewPagerAdapter.getMembershipFormFragment().getFirstName());
//                params.put("player_on_loan",viewPagerAdapter.getMembershipFormFragment())
                params.put("club",viewPagerAdapter.getMembershipFormFragment().getClub());
                params.put("division",viewPagerAdapter.getMembershipFormFragment().getDivision());
                params.put("contract_start",viewPagerAdapter.getMembershipFormFragment().getContractStarts());
                params.put("contract_ends",viewPagerAdapter.getMembershipFormFragment().getContractEnds());
//                params.put("free_agent",viewPagerAdapter.getContactFormFragment()
                params.put("address",viewPagerAdapter.getContactFormFragment().getAddress());
                params.put("city",viewPagerAdapter.getContactFormFragment().getCity());
                params.put("postal_code",viewPagerAdapter.getContactFormFragment().getPostalCode());
                params.put("gsm",viewPagerAdapter.getContactFormFragment().getGSM());
                params.put("country",viewPagerAdapter.getContactFormFragment().getCountry());
                params.put("email",viewPagerAdapter.getContactFormFragment().getEmail());
                params.put("phone",viewPagerAdapter.getContactFormFragment().getPhone());
                params.put("fax",viewPagerAdapter.getContactFormFragment().getFax());
                params.put("twitter",viewPagerAdapter.getContactFormFragment().getTwitter());
                params.put("other",viewPagerAdapter.getContactFormFragment().getOther());
                params.put("facebook",viewPagerAdapter.getContactFormFragment().getFacebook());
//                params.put("player_is_minor",viewPagerAdapter.getContactFormFragment());
                params.put("has_an_agent",viewPagerAdapter.getFootballerFormFragment().hasAnAgent()?"1":"0");
                params.put("position",viewPagerAdapter.getFootballerFormFragment().getPosition());
                params.put("national_team",viewPagerAdapter.getFootballerFormFragment().getNationalTeam());
                params.put("under_16",viewPagerAdapter.getFootballerFormFragment().isUnder16()?"1":"0");
                params.put("under_17",viewPagerAdapter.getFootballerFormFragment().isUnder17()?"1":"0");
                params.put("under_18",viewPagerAdapter.getFootballerFormFragment().isUnder18()?"1":"0");
                params.put("under_19",viewPagerAdapter.getFootballerFormFragment().isUnder19()?"1":"0");
                params.put("20_years",viewPagerAdapter.getFootballerFormFragment().is20Years()?"1":"0");
                params.put("other_selections",viewPagerAdapter.getFootballerFormFragment().getOtherSelections());
                params.put("olympics",viewPagerAdapter.getFootballerFormFragment().getOlypmics());
                params.put("height_cm",viewPagerAdapter.getAdditionalFormFragment().getHeightCM());
                params.put("height_m",viewPagerAdapter.getAdditionalFormFragment().getHeightM());
                params.put("weight",viewPagerAdapter.getAdditionalFormFragment().getWeight());
                params.put("size",viewPagerAdapter.getAdditionalFormFragment().getSize());
                params.put("left_footer",viewPagerAdapter.getAdditionalFormFragment().isLeftFooter()?"1":"0");
                params.put("right_footer",viewPagerAdapter.getAdditionalFormFragment().isRightFooter()?"1":"0");
                params.put("both_feet",viewPagerAdapter.getAdditionalFormFragment().isBothFeet()?"1":"0");
                params.put("place_of_birth",viewPagerAdapter.getAdditionalFormFragment().getPlaceOfBirth());
                params.put("date_of_birth",viewPagerAdapter.getAdditionalFormFragment().getDateOfBirth());
                params.put("nationality",viewPagerAdapter.getAdditionalFormFragment().getNationality());
                params.put("number_of_children",viewPagerAdapter.getAdditionalFormFragment().getNumberOfChildren());
                params.put("marital_status",viewPagerAdapter.getAdditionalFormFragment().getMaritalStatus());
                return params;
            }
        };

// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            List<Image> images = ImagePicker.getImages(data);
            // or get a single image only
            Image image = ImagePicker.getFirstImageOrNull(data);


        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }


    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getContractStarts() {
        return contractStarts;
    }

    public void setContractStarts(String contractStarts) {
        this.contractStarts = contractStarts;
    }

    public String getContractEnds() {
        return contractEnds;
    }

    public void setContractEnds(String contractEnds) {
        this.contractEnds = contractEnds;
    }

    public boolean isPlayerOnLoan() {
        return playerOnLoan;
    }

    public void setPlayerOnLoan(boolean playerOnLoan) {
        this.playerOnLoan = playerOnLoan;
    }

    public boolean isFreeAgent() {
        return freeAgent;
    }

    public void setFreeAgent(boolean freeAgent) {
        this.freeAgent = freeAgent;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getGsm() {
        return gsm;
    }

    public void setGsm(String gsm) {
        this.gsm = gsm;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public boolean isPlayerIsMinor() {
        return playerIsMinor;
    }

    public void setPlayerIsMinor(boolean playerIsMinor) {
        this.playerIsMinor = playerIsMinor;
    }

    public boolean isHasAnAgent() {
        return hasAnAgent;
    }

    public void setHasAnAgent(boolean hasAnAgent) {
        this.hasAnAgent = hasAnAgent;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getNationalTeam() {
        return nationalTeam;
    }

    public void setNationalTeam(String nationalTeam) {
        this.nationalTeam = nationalTeam;
    }

    public boolean isUnder16() {
        return under16;
    }

    public void setUnder16(boolean under16) {
        this.under16 = under16;
    }

    public boolean isUnder17() {
        return under17;
    }

    public void setUnder17(boolean under17) {
        this.under17 = under17;
    }

    public boolean isUnder18() {
        return under18;
    }

    public void setUnder18(boolean under18) {
        this.under18 = under18;
    }

    public boolean isUnder19() {
        return under19;
    }

    public void setUnder19(boolean under19) {
        this.under19 = under19;
    }

    public boolean is_20years() {
        return _20years;
    }

    public void set_20years(boolean _20years) {
        this._20years = _20years;
    }

    public String getOtherSelection() {
        return otherSelection;
    }

    public void setOtherSelection(String otherSelection) {
        this.otherSelection = otherSelection;
    }

    public String getOlympic() {
        return olympic;
    }

    public void setOlympic(String olympic) {
        this.olympic = olympic;
    }

    public String getHeightM() {
        return heightM;
    }

    public void setHeightM(String heightM) {
        this.heightM = heightM;
    }

    public String getHeightCM() {
        return heightCM;
    }

    public void setHeightCM(String heightCM) {
        this.heightCM = heightCM;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public boolean isLeftFooter() {
        return leftFooter;
    }

    public void setLeftFooter(boolean leftFooter) {
        this.leftFooter = leftFooter;
    }

    public boolean isRightFooter() {
        return rightFooter;
    }

    public void setRightFooter(boolean rightFooter) {
        this.rightFooter = rightFooter;
    }

    public boolean isBothFeet() {
        return bothFeet;
    }

    public void setBothFeet(boolean bothFeet) {
        this.bothFeet = bothFeet;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }
}
