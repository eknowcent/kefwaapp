package com.apps.developer.kefwaapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.developer.kefwaapp.R;

public class RegistrationFormFragment extends Fragment {
    private View view=null;
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle savedInstanceState){
        view=layoutInflater.inflate(R.layout.fragment_registration_form,viewGroup,false);
        return view;
    }
}
