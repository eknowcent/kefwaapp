package com.apps.developer.kefwaapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.apps.developer.kefwaapp.R;

public class ContactFormFragment extends Fragment {
    private View view=null;
    private EditText etAddress=null;
    private EditText etCity=null;
    private EditText etPostalCode=null;
    private EditText etGSM=null;
    private Spinner sCountry=null;
    private EditText etEmail=null;
    private EditText etPhone=null;
    private EditText etFax=null;
    private EditText etTwitter=null;
    private EditText etOther=null;
    private EditText etFacebook=null;
    private RadioGroup rgPlayerIsMinor=null;
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle savedInstanceState){
        view=layoutInflater.inflate(R.layout.fragment_contact_form,viewGroup,false);
        initFields();
        return view;
    }
    public void initFields(){
        etAddress=(EditText)view.findViewById(R.id.et_address);
        etCity=(EditText)view.findViewById(R.id.et_city);
        etPostalCode=(EditText)view.findViewById(R.id.et_postal_code);
        etGSM=(EditText)view.findViewById(R.id.et_gsm);
        sCountry=(Spinner)view.findViewById(R.id.et_country);
        etEmail=(EditText)view.findViewById(R.id.et_email);
        etPhone=(EditText)view.findViewById(R.id.et_phone);
        etFax=(EditText)view.findViewById(R.id.et_fax);
        etTwitter=(EditText)view.findViewById(R.id.et_twitter);
        etOther=(EditText)view.findViewById(R.id.et_other);
        etFacebook=(EditText)view.findViewById(R.id.et_facebook);
        rgPlayerIsMinor=(RadioGroup)view.findViewById(R.id.rg_player_is_minor);

    }
    public String getAddress(){
        return etAddress.getText().toString();
    }
    public String getCity(){
        return etCity.getText().toString();
    }
    public String getPostalCode(){
        return etPostalCode.getText().toString();
    }
    public String getGSM(){
        return etGSM.getText().toString();
    }
    public String getCountry(){
        return sCountry.getSelectedItem().toString();
    }
    public String getEmail(){
        return etEmail.getText().toString();
    }
    public String getPhone(){
        return etPhone.getText().toString();
    }
    public String getFax(){
        return etFax.getText().toString();
    }
    public String getTwitter(){
        return etTwitter.getText().toString();
    }
    public String getFacebook(){
        return etFacebook.getText().toString();
    }
    public String getOther(){
        return etOther.getText().toString();
    }
}
